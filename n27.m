clc
clear all
%��������� ��������##############################################
N=4; %������ �������
r=50; %������������ �������� �������� �����������
V = 250; %�/�
psi_grad = 90; %�� ��� � ������ �������
Y_true_begin = 1000; %�����
X_true_begin = 5000; %�����
%###############################################################

points_true = Point;
psi = psi_grad*pi/180;
for i=1:N
    Vy = V*sin(psi);
    Vx = V*cos(psi);
    x = X_true_begin + (i-1)*Vx;
    y = Y_true_begin + (i-1)*Vy;
    points_true(i) = Point(x, y); 
end %������ �������� �������� X

points = Point;

for i=1:N
    p = rand()*r;
    fi = rand()*2*pi;
    a = p*cos(fi);
    e = p*sin(fi);     
    points(i) = points_true(i).Offset(a,e);
end %����������� �����������



%������� ��������--------------------------------------
Point.Plot(points_true, '*');
axis image
%axis([min(Y_true)-V max(Y_true)+V min(X_true)-V max(X_true)+V])
hold on
Point.Plot(points, 'x');
Point.PlotCircle(points, r);
grid on
grid minor
xlabel('���������� Y, �') %���������� �� ������
ylabel('���������� X, �') %���������� �� �����
%################################################################

speeds = Speed();
twoPoints_short=0; %����� ����������� �������� ���������
twoPoints_long=0; %����� ������������ �������� ���������
twoPoints_down=0; %����� ������ ������
twoPoints_up = 0; %����� ������� ������
b_max = 0;
b_min = 0;
nextPoints_short = List(Point);%�������������� ����� ��������
nextPointsShortLists = List();
nextPoints_long = List(Point);%�������������� ����� ���������
nextPointsLongLists = List();
index = 0;

%for I=1:1
for I=1:N-1
    for J=I+1:N
    %for J=N:N
        Lengths = zeros(1, J-I+1);
        MinX_index = -1;
        MaxX_index = -1;
        MinLength = inf;
        MaxLength = -1;
        for i=I:J    
            Lengths(i) = points(i).GetLength(psi);
            if(Lengths(i)<MinLength)
                MinLength = Lengths(i);
                MinX_index = i;
            end

            if(Lengths(i)> MaxLength)
                MaxLength = Lengths(i);
                 MaxX_index = i;
            end
        end %���������� �� ����� �����

        %##########################################################
        
        point1_up = Point;
        point2_up = Point;
        point1_up.Y = points(I).Y - V;
        point2_up.Y = points(J).Y + V;
        Xkr1 = r*cos(psi-0.5*pi)+points(MinX_index).X;
        Yrk1 = r*sin(psi-0.5*pi)+points(MinX_index).Y;
        XXXmax = cot(psi)*Yrk1+MinLength;
        b_max_current = Xkr1 - XXXmax + MinLength;
        b_max = [b_max, b_max_current];
        point1_up.X = cot(psi)*point1_up.Y+b_max_current;
        point2_up.X = cot(psi)*point2_up.Y+b_max_current;
        twoPoints_up = [twoPoints_up twoPoints(point1_up, point2_up)];
        %Point.Plot([point1_up point2_up], '-');
        %������� �����������#######################################

        %############################################################
        point1_down = Point;
        point2_down = Point;
        point1_down.Y = points(I).Y - V;
        point2_down.Y = points(J).Y + V;
        Xkr2 = r*cos(psi+0.5*pi)+points(MaxX_index).X;
        Yrk2 = r*sin(psi+0.5*pi)+points(MaxX_index).Y;
        XXXmin = cot(psi)*Yrk2 + MaxLength;
        b_min_current =  Xkr2 - XXXmin + MaxLength;
        b_min = [b_min, b_min_current];
        point1_down.X = cot(psi)* point1_down.Y + b_min_current;
        point2_down.X = cot(psi)* point2_down.Y + b_min_current;
        twoPoints_down = [twoPoints_down twoPoints(point1_down, point2_down)];
        %Point.Plot([point1_down point2_down], '-');
        %������ �����������##############################################

        to4nost = 360;
        dt = pi/to4nost;

        t1 = [psi-0.5*pi: dt: psi+0.5*pi];
        t2 = [psi+1.5*pi: -dt:  psi+0.5*pi]; 
        [PointsK1, PointsKN] = getPoints(t1,t2,r,points(I), points(J), psi, b_max_current, b_min_current);
        para_points_short = getParaPoints(PointsK1, PointsKN, psi); %���� ����� �������� �����

        sz3 = size(para_points_short);
        min_len = inf;
        point1_short = para_points_short(1).point1; %������ ������ ��������� ����
        point2_short = para_points_short(1).point2; %����� ������ ��������� ����
        min_i = -1;

        for i = 1: sz3(1, 2) 
            x1 = para_points_short(i).point1.X;
            y1 = para_points_short(i).point1.Y;
            x2 = para_points_short(i).point2.X;
            y2 = para_points_short(i).point2.Y;
            len = sqrt((x1-x2)^2+(y1-y2)^2);
            Vtemp = len / (J-I);
            Vy = Vtemp*sin(psi);
            Vx = Vtemp*cos(psi);
            nextPoints_short = nextPoints_short.add(para_points_short(i).point2.Offset(Vx, Vy));
            if(min_len>len)
                min_len = len;
                point1_short =  para_points_short(i).point1;
                point2_short = para_points_short(i).point2;
            end
        end  
        nextPointsShortLists = nextPointsShortLists.add(nextPoints_short);
        nextPoints_short = nextPoints_short.clear();
        twoPoints_short = [twoPoints_short twoPoints(point1_short, point2_short)];
        
        Vmin = min_len/(J-I);
        sprintf('����������� �������� %i-%i =%.2f  �/�', +I,+J,+Vmin)

        t11 = [psi+0.5*pi: dt: psi+1.5*pi];
        t22 = [psi-0.5*pi: dt:  psi+0.5*pi]; 
        [PointsK1, PointsKN] = getPoints(t11,t22,r,points(I), points(J), psi, b_max_current, b_min_current);
        para_points_long = getParaPoints(PointsK1, PointsKN, psi);% ���� ����� ������� �����

        max_len = -1;
        point1_long = para_points_long(1).point1; %������ ������ �������� ����
        point2_long = para_points_long(1).point2; %����� ������ �������� ����
        sz3 = size(para_points_long);
        for i = 1: sz3(1, 2) 
            x1 = para_points_long(i).point1.X;
            y1 = para_points_long(i).point1.Y;
            x2 = para_points_long(i).point2.X;
            y2 = para_points_long(i).point2.Y;
            len = sqrt((x1-x2)^2+(y1-y2)^2);
            Vtemp = len / (J-I);
            Vy = Vtemp*sin(psi);
            Vx = Vtemp*cos(psi);
            nextPoints_long = nextPoints_long.add(para_points_long(i).point2.Offset(Vx, Vy));
            if(max_len<len)
                max_len = len;
                point1_long =  para_points_long(i).point1;
                point2_long =  para_points_long(i).point2;
            end
        end
        nextPointsLongLists = nextPointsLongLists.add(nextPoints_long);
        nextPoints_long = nextPoints_long.clear();      
        twoPoints_long = [twoPoints_long twoPoints(point1_long, point2_long)];        

        Vmax = max_len/(J-I);
        sprintf('������������ �������� %i-%i =%.2f  �/�', +I,+J,+Vmax)
        txt = strcat(num2str(I), '-', num2str(J));
        speeds = [speeds Speed(Vmin, Vmax, txt)];
    end  
    index = index + 1;
end


b_min(1) = [];
b_max(1) = [];
speeds(1) = [];
twoPoints_short(1) = [];
twoPoints_long(1) = [];
twoPoints_down(1) = [];
twoPoints_up(1) = [];

[speed_min, speed_max] = Speed.getMinMax(speeds);
sprintf('������������ �������� %.2f =   �/�', +speed_max)
sprintf('����������� �������� %.2f =   �/�', +speed_min)

Point.Plot([twoPoints_down(index).point1 twoPoints_down(index).point2], '-')
Point.Plot([twoPoints_up(index).point1 twoPoints_up(index).point2], '-')
Point.Plot([twoPoints_long(index).point1 twoPoints_long(index).point2], '-s')
Point.Plot([twoPoints_short(index).point1 twoPoints_short(index).point2], '-d')


nextPointsMinLists_arr = nextPointsShortLists.getArr();
arr = nextPointsMinLists_arr{index}.getArr();
nextPointsMaxLists_arr = nextPointsLongLists.getArr();
arr2 = nextPointsMaxLists_arr{index}.getArr();
sz = size(arr2);
sz = sz(2);
if(arr2(1).GetLength(psi)>arr2(sz).GetLength(psi))
    arr2 = fliplr(arr2);
end

Point.Fill([arr arr2], 'y');
alpha(1);


%������������� ��������������� #################################
boxes(1) = Box();
for i=1:N
    [Ymin, Ymax] = MinMax(points(i), r, b_max(index), b_min(index));
    p1 = Point(b_min(index), Ymin);
    p2 = Point(b_max(index), Ymin);
    p3 = Point(b_max(index), Ymax);
    p4 = Point(b_min(index), Ymax);
    boxes(i) = Box(p1,p2,p3,p4);    
end

[p1, p2, p3, p4] = BoxAppr([arr, arr2]);
boxes(N+1) = Box(p1,p2,p3,p4);
sz = size(boxes);
sz = sz(2);
boxes_orig = boxes;
%Box.Fill(boxes_orig, 'g');
alpha(0.5);

for i = sz:-1:2
    p1 = Point(b_min(index), boxes(i).point1.Y-speed_max);
    p2 = Point(b_max(index), boxes(i).point1.Y-speed_max);
    p3 = Point(b_max(index), boxes(i).point4.Y-speed_min);
    p4 = Point(b_min(index), boxes(i).point4.Y-speed_min);
    if p1.Y<boxes(i-1).point1.Y
        p1 = boxes(i-1).point1;
        p2 = boxes(i-1).point2;
    end

    if p3.Y>boxes(i-1).point3.Y
        p3 = boxes(i-1).point3;
        p4 = boxes(i-1).point4;
    end 
    boxes(i-1) = Box(p1,p2,p3,p4); 
end

boxes_better = boxes;
%Box.Fill(boxes_better, 'r');
alpha(0.5);

for i = 1:sz-1
    p1 = Point(b_min(index), boxes(i).point1.Y+speed_min);
    p2 = Point(b_max(index), boxes(i).point1.Y+speed_min);
    p3 = Point(b_max(index), boxes(i).point4.Y+speed_max);
    p4 = Point(b_min(index), boxes(i).point4.Y+speed_max);
    if p1.Y<boxes(i+1).point1.Y
        p1 = boxes(i+1).point1;
        p2 = boxes(i+1).point2;
    end

    if p3.Y>boxes(i+1).point3.Y
        p3 = boxes(i+1).point3;
        p4 = boxes(i+1).point4;
    end 
    boxes(i+1) = Box(p1,p2,p3,p4); 
end

Box.Fill(boxes, 'b');
alpha(0.5);

%��������� � ��� �� �� ��������----------------------------------
x_arr = zeros(1,N);
y_arr = zeros(1,N);
for i=1:N
    x_arr(i) = points(i).X;
    y_arr(i) = points(i).Y;
end
 p1 = polyfit(y_arr, x_arr, 0);
 xx = polyval(p1, y_arr);
plot(y_arr, xx);
 
x_min = min(x_arr);
x_max = max(x_arr);
dx = (x_max - x_min)/100;
x_current = x_min;
x_mnk = 0;
sum_min = inf;

while x_current <= x_max
    sum = 0;
    for j=1:N
        sum = sum + (x_arr(j) - x_current)^2;
    end
    
    if(sum<sum_min)
        sum_min = sum;
        x_mnk = x_current;
    end
    x_current = x_current + dx;
end
y1_mnk_complex = sqrt(r^2-(x_mnk - x_arr(1))^2)+y_arr(1);
y1_mnk = real(y1_mnk_complex);
y1_min_len = y1_mnk;
y1_max_len = 2*y_arr(1) - y1_min_len;  

yN_mnk_complex = sqrt(r^2-(x_mnk - x_arr(N))^2)+y_arr(N);
yN_mnk = real(yN_mnk_complex);
yN_max_len = yN_mnk;
yN_min_len = 2*y_arr(N) - yN_max_len;  

len_min = yN_min_len - y1_min_len;
len_max = yN_max_len - y1_max_len;
V_min_mnk = len_min/(N-1);
V_max_mnk = len_max/(N-1);
sprintf('����������� �������� ��� %.2f =   �/�', +V_min_mnk)
sprintf('������������ �������� ��� %.2f =   �/�', +V_max_mnk)
plot([y1_min_len, yN_min_len], [x_mnk, x_mnk] ,'LineWidth',3);
plot([y1_max_len, yN_max_len], [x_mnk, x_mnk], '--', 'LineWidth',2);
%--------------------------------------------------------------

figure();
hold on;
Speed.Plot(speeds);
sz = size(speeds); sz = sz(2);
plot([V V], [0, sz+1], '--');
plot([speed_min, speed_min], [0 sz+1], '-');
plot([speed_max, speed_max], [0 sz+1], '-');
xlabel('�������� , �/�') %���������� �� ������


