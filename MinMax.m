function [ minY maxY ] = MinMax( point, r, up_limit, down_limit )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    t=[0:pi/180:2*pi];                         
    X_krug=r*cos(t)+point.X; 
    Y_krug=r*sin(t)+point.Y; 
    minY = inf;
    maxY = -inf;
    sz = size(X_krug);
    for i =1:sz(2)
        if X_krug(i)<= up_limit && X_krug(i)>=down_limit
            if Y_krug(i)> maxY
                maxY = Y_krug(i);
            end
            
            if Y_krug(i) < minY
                minY = Y_krug(i);
            end
        end
    end
end

