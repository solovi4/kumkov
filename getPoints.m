function [ pointsK1_wz, pointsKN_wz ] = getPoints( t1, t2, r, point1, point2, psi, b_max, b_min )
%����� �� �����������
%   t1 � t2 ��� ����� �������� �����
    x1 = point1.X;
    y1 = point1.Y;
    xn = point2.X;
    yn = point2.Y;
    X_krug1=r*cos(t1)+x1; 
    Y_krug1=r*sin(t1)+y1;
  
    pointsK1 = Point;
    CountXpodx1 = 0;
    for j=1: length(Y_krug1)
        X_max_k1 = cot(psi)*Y_krug1(j)+b_max;
        X_min_k1 = cot(psi)*Y_krug1(j)+b_min;
        if (X_krug1(j)>= X_min_k1) && (X_krug1(j)<=X_max_k1)
            pointsK1(j) = Point(X_krug1(j), Y_krug1(j));
            CountXpodx1 = CountXpodx1+1;
        end
    end



    X_krugN=r*cos(t2)+xn; 
    Y_krugN=r*sin(t2)+yn;
    pointsKN = Point;
    CountXpodxN = 0;
    for j=1: length(Y_krugN)
        X_max_kN = cot(psi)*Y_krugN(j)+b_max;
        X_min_kN = cot(psi)*Y_krugN(j)+b_min;
        if (X_krugN(j)>= X_min_kN) && (X_krugN(j)<=X_max_kN)
            pointsKN(j) = Point( X_krugN(j), Y_krugN(j));
            CountXpodxN = CountXpodxN + 1;
        end    
    end
    
    pointsK1_wz = Point;    
    sz = size(pointsK1);
    counter = 1;
    for i=1:sz(2)
        if(pointsK1(i).X ~= 0 && pointsK1(i).Y ~= 0)
            pointsK1_wz(counter) = pointsK1(i);
            counter = counter + 1;
        end
    end

    pointsKN_wz = Point;  
    counter = 1;
    sz = size(pointsKN);
    for i=1:sz(2)
        if(pointsKN(i).X ~= 0 && pointsKN(i).Y ~= 0)
            pointsKN_wz(counter) = pointsKN(i);
            counter = counter + 1;
        end
    end
   
end

