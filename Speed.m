classdef Speed
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Vmin
        Vmax
        title
    end
    
    methods  
        function obj = Speed(V_min, V_max, title)
             if(nargin == 3)
                 obj.Vmin = V_min;
                 obj.Vmax = V_max;
                 obj.title = title;
             else
                 obj.Vmin = 0;
                 obj.Vmax = 0;
                 obj.title = '';
             end
        end
    end
    
    methods(Static)  
        function Plot(speeds)
            sz = size(speeds);             
            y_start = sz(2)+1;    
            for i=1:sz(2)
                x1 = speeds(i).Vmin;
                x2 = speeds(i).Vmax;
                x_arr = [x1, x2];
                y_arr = [y_start-i, y_start-i];
                plot(x_arr, y_arr, '-');
                text(x2, y_arr(2), speeds(i).title);
            end
        end
        
        function [speed_min speed_max] = getMinMax(speeds) %Минимальный максимум, максимальный минимум            
            speed_max = inf;
            speed_min = -inf;
            sz = size(speeds);
            for i = 1:sz(2)
                if speed_max>speeds(i).Vmax
                    speed_max = speeds(i).Vmax;
                end

                if speed_min<speeds(i).Vmin
                    speed_min = speeds(i).Vmin;
                end
            end            
        end
        
    end
    
end

