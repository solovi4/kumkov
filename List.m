classdef List
    %������������ ������
    %   Detailed explanation goes here
    
    properties
        arr
        length
    end
    
    methods
        function obj = List(object)
            if(nargin ~= 0)                              
                if(~isa(object,'List'))
                    obj.length  = 0;  
                    obj.arr = object;
                else
                    obj.length = 0;
                    obj.arr = cell(1);
                end            
            else
                obj.length = 0;
                obj.arr = cell(1);
            end
        end
        
        function obj = add(obj, element)
            if(~isa(element, 'List'))
                obj.length = obj.length + 1;
                obj.arr(obj.length) = element;
            else
                 obj.length = obj.length + 1;
                obj.arr{obj.length} = element;
            end
        end
        
        function array = getArr(obj)
            array = obj.arr;
        end
        
        function obj = clear(obj)
            if(~isa(obj.arr, 'List'))
                 obj.arr = obj.arr(1);
                obj.length = 0;
            else
                 obj.arr = cell(1);
                obj.length = 0;
            end
           
        end
    end
    
end

