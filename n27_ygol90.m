clc
N=4; %������ �������
r=50; %������������ �������� �������� �����������
V = 250; %�/�
psi_grad = 90; %�� ��� � ������ �������
X_true = zeros(1,N); %� ��������
Y_true = zeros(1,N); %Y ��������
X_true_begin = 1000; %�����
Y_true_begin = 5000; %�����
psi = psi_grad*pi/180;
for i=1:N
    Vy = V*sin(psi);
    Y_true(i) = X_true_begin + (i-1)*Vy;
end %������ �������� �������� X

for i=1:N
    Vx = V*cos(psi);
    X_true(i) = Y_true_begin + (i-1)*Vx;
end %������ �������� �������� Y

%������� �������� ��������--------------------------------------
plot(Y_true, X_true, '*');
axis image
axis([min(Y_true)-V max(Y_true)+V min(X_true)-V max(X_true)+V])

grid on
xlabel('���������� Y, �') %���������� �� ������
ylabel('���������� X, �') %���������� �� �����
%################################################################

X = zeros(1, N); %������ X � �������������
Y = zeros(1, N); %������ Y � �������������
a = zeros(1, N); %����������� ��������� �� X
e = zeros(1, N); %����������� ��������� �� Y

for i=1:N
    p = rand()*r;
    fi = rand()*2*pi;
    a(i) = p*cos(fi);
    e(i) = p*sin(fi);
    X(i) = X_true(i) + a(i);
    Y(i) = Y_true(i) + e(i);
end

hold on
plot(Y, X, 'x');

for i=1:N
    t=[0:pi/180:2*pi];
    X_krug=r*cos(t)+X(i); 
    Y_krug=r*sin(t)+Y(i); 
    plot(Y_krug, X_krug); 
end %����� ������ ���������� ��������


X_max = [min(X)+r,  min(X)+r];
X_min = [max(X)-r, max(X)-r];

%plot([Y(1)-V Y(N)+V], X_max);
%plot([Y(1)-V Y(N)+V], X_min);

dX = (X_max-X_min)/1000;
Len_max=-1;
Y_min_Vmax=-1;
Y_max_Vmax=-1;
X_Vmax=-1;
for i=X_min(1):dX:X_max(1)
    X_Vmax_temp = i;
    Y_min_temp = Y(1) - r*sqrt(1-((X_Vmax_temp-X(1))/r)^2);
    Y_max_temp = Y(N) + r*sqrt(1-((X_Vmax_temp-X(N))/r)^2);   
    Len = Y_max_temp - Y_min_temp;
    if(Len>Len_max)
        Len_max = Len;
        Y_min_Vmax = Y_min_temp;
        Y_max_Vmax = Y_max_temp;
        X_Vmax = X_Vmax_temp;
    end
end %������� �������� ������ ������������ ���������� �����
Y_Vmax = [Y_min_Vmax, Y_max_Vmax];
X_Vmax_arr = [X_Vmax, X_Vmax];
plot(Y_Vmax, X_Vmax_arr, 'o-');
Vmax = (Y_max_Vmax-Y_min_Vmax)/(N-1);
sprintf('������������ ��������=%.2f  �/�', +Vmax)

Y_min_Vmin=-1;
Y_max_Vmin=-1;
X_Vmin=-1
Len_min=inf;
for i=X_min(1):dX:X_max(1)
    X_Vmin_temp = i;
    Y_min_temp = Y(1) + r*sqrt(1-((X_Vmin_temp-X(1))/r)^2);
    Y_max_temp = Y(N) - r*sqrt(1-((X_Vmin_temp-X(N))/r)^2);   
    Len = Y_max_temp - Y_min_temp;
    if(Len<Len_min)
        Len_min = Len;
        Y_min_Vmin = Y_min_temp;
        Y_max_Vmin = Y_max_temp;
        X_Vmin = X_Vmin_temp;
    end
end %������� �������� ������ ����������� ���������� �����

Y_Vmin = [Y_min_Vmin, Y_max_Vmin];
X_Vmin_arr = [X_Vmin, X_Vmin];
plot(Y_Vmin, X_Vmin_arr, '^-');
Vmin = (Y_max_Vmin-Y_min_Vmin)/(N-1);
sprintf('����������� ��������=%.2f  �/�', +Vmin)

