classdef Box
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        point1
        point2
        point3
        point4
    end
    
    methods
        function obj = Box(p1, p2, p3, p4)
             if(nargin == 4)                     
                 obj.point1 = p1;
                 obj.point2 = p2;
                 obj.point3 = p3;
                 obj.point4 = p4;
             else
                obj.point1 = 0;
                 obj.point2 = 0;
                 obj.point3 = 0;
                 obj.point4 = 0;
             end
        end
    end
    
    methods(Static)
        function Fill(boxes, str)
            sz = size(boxes);
            x_arr = zeros(1,4);
            y_arr = zeros(1,4);
            for i=1: sz(2)
                x_arr(1) = boxes(i).point1.X;
                x_arr(2) = boxes(i).point2.X;
                x_arr(3) = boxes(i).point3.X;
                x_arr(4) = boxes(i).point4.X;
                y_arr(1) = boxes(i).point1.Y;
                y_arr(2) = boxes(i).point2.Y;
                y_arr(3) = boxes(i).point3.Y;
                y_arr(4) = boxes(i).point4.Y;
                fill(y_arr, x_arr, str);
            end            
            
        end
    end
    
end

