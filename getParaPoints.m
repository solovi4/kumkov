function [ para_points ] = getParaPoints( points1, points2, angle )
% ���������� ���� �����
%   ������� ����� �� ����� ������
sz = size(points1);
sz2 = size(points2);

if sz(2)<=sz2(2)
    P1 = points1;
    P2 = points2;
else
    P1 = points2;
    P2 = points1;
end

l1 = size(P1);
l2 = size(P2);
l1 = l1(2);
l2 = l2(2);
%para_points = cell(2, l1);
para_points = twoPoints();
counter_pp = 1;
min_razn = inf;

for i = 1: l1      
    len1 = P1(i).GetLength(angle);
    for j = 1: l2
        try
            len2 = P2(j).GetLength(angle);
            razn = abs(len1 - len2);
            if min_razn> razn
                min_razn = razn;  
                para_points(counter_pp).point1 = P1(i);
                para_points(counter_pp).point2 = P2(j);
            else                 
                break;
            end
        catch ME
            disp(ME);
        end        
    end
    counter_pp = counter_pp +1;
    min_razn = inf;
end

if sz(2)>sz2(2)
    for i=1:counter_pp-1
        buf = para_points(i).point1;
        para_points(i).point1 = para_points(i).point2;
        para_points(i).point2 = buf;
    end
end

end

