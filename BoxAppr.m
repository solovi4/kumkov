function [ point1, point2, point3, point4 ] = BoxAppr( points )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    sz = size(points);
    minY = inf;
    minX = inf;
    maxY = -inf;
    maxX = -inf;
    for i=1:sz(2)
        if points(i).Y < minY
            minY = points(i).Y;
            point1.Y = minY;
            point2.Y = minY;
        end
        
        if points(i).Y > maxY
            maxY = points(i).Y;
            point3.Y = maxY;
            point4.Y = maxY;
        end
        
        if points(i).X < minX
            minX = points(i).X;
            point1.X = minX;
            point4.X = minX;
        end        
        
        if points(i).X > maxX
            maxX = points(i).X;
            point2.X = maxX;
            point3.X = maxX;
        end
    end

end

