classdef Point
    %   �����
    %   � � �
    
    properties
        X
        Y
    end
    
    methods
        function obj = Point(x, y)
            if(nargin ~= 2)
                obj.X = 0;
                obj.Y = 0;
            elseif (nargin == 2) 
                obj.X = x;
                obj.Y = y;
            end
        end  % �����������
        
        function obj = Offset(obj, x_offset, y_offset)
            obj.X = obj.X + x_offset;
            obj.Y = obj.Y + y_offset;
        end %�������� �����
        
        function Length = GetLength(obj, angle)
            Bx = obj.X;
            Ax = cot(angle)*obj.Y;
            AB = Bx - Ax;
            Length = abs(AB);
        end
    end
      
    methods(Static)
        function Plot(points, str)
            sz = size(points);
            x_arr = zeros(sz);
            y_arr = zeros(sz);
            for i=1: sz(2)
                x_arr(i) = points(i).X;
                y_arr(i) = points(i).Y;
            end            
            plot(y_arr, x_arr, str);
        end
        
        function Fill(points, str)
            sz = size(points);
            x_arr = zeros(sz);
            y_arr = zeros(sz);
            for i=1: sz(2)
                x_arr(i) = points(i).X;
                y_arr(i) = points(i).Y;
            end            
            fill(y_arr, x_arr, str);
        end
        
        function PlotCircle(points, radius)
            sz = size(points);
            t=[0:pi/180:2*pi];
            for i=1: sz(2)               
                X_krug=radius*cos(t)+points(i).X; 
                Y_krug=radius*sin(t)+points(i).Y; 
                plot(Y_krug, X_krug); 
            end   
        end
    end
    
    
end

