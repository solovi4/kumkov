classdef twoPoints
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        point1
        point2
    end
    
    methods
        function obj = twoPoints(p1, p2)
            if(nargin == 2)
                 obj.point1 = p1;
                 obj.point2 = p2;
            else
                 obj.point1 = Point();
                 obj.point2 = Point();
             end
        end
    end
    
end

